<?php
abstract class Hewan{
    public $nama, $darah, $jumlahKaki, $keahlian, $darahEnemy;

    public function atraksi($atr){
        return $this->nama." ".$atr;
    }
}

abstract class Fight extends Hewan {
    public $attackPower, $defencePower;


    public function serang($selfAtt, $enemyDef, $enemyName, $enemyDarah){
        $this->darahEnemy = $this->sisa_darah($enemyDarah, $selfAtt, $enemyDef);
        return $this->nama." sedang menyerang ".$enemyName ."\n" .$this->diserang($enemyName);
    }

    public function diserang($enemyName){
        return $enemyName. " sedang diserang oleh ".$this->nama;
    }

    public function sisa_darah($enemyDarah, $selfAtt, $enemyDef){
        return ($enemyDarah - $selfAtt) / $enemyDef;
    }
}

class Elang extends Fight {
    public function __construct($nama){
        $this->nama = $nama;
        $this->darah = 50;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
        $this->darahEnemy = 0;
    }

    public function getInfoHewan(){
        return "Nama : " . $this->nama . "\n" .
                "Darah : " . $this->darah . "\n" .
                "Jumlah Kaki : " . $this->jumlahKaki . "\n" .
                "Keahlian : " . $this->keahlian . "\n" .
                "Attack Power : " . $this->attackPower . "\n" .
                "Defence Power : " . $this->defencePower . "\n";
    }

}

class Harimau extends Fight {
    public function __construct($nama){
        $this->nama = $nama;
        $this->darah = 50;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function getInfoHewan(){
        return "Nama : " . $this->nama . "\n" .
                "Darah : " . $this->darah . "\n" .
                "Jumlah Kaki : " . $this->jumlahKaki . "\n" .
                "Keahlian : " . $this->keahlian . "\n" .
                "Attack Power : " . $this->attackPower . "\n" .
                "Defence Power : " . $this->defencePower . "\n";
    }

}

$elang = new Elang("elang");
$harimau = new Harimau("harimau");

echo $elang->getInfoHewan();
echo PHP_EOL;
echo $harimau->getInfoHewan();

echo PHP_EOL;
// elang menyerang
echo $elang->serang($elang->attackPower, $harimau->defencePower, $harimau->nama, $harimau->darah);
echo PHP_EOL;
$harimau->darah = $elang->darahEnemy;
echo $harimau->getInfoHewan();


echo PHP_EOL;
// harimau menyerang
echo $harimau->serang($harimau->attackPower, $elang->defencePower, $elang->nama, $elang->darah);
echo PHP_EOL;
$elang->darah = $harimau->darahEnemy;
echo $elang->getInfoHewan();