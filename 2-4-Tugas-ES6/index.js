//soal 1
const luasPersegi = (p,l) => {
    return p*l
}

console.log(luasPersegi(2,6))  //jawaban 

//soal 2
const newFunction = (firstName, lastName) => {
    return {
        fullName: () => {
        console.log(`${firstName} ${lastName}`)
        }
    }
}

newFunction("William", "Imoh").fullName() //jawaban 

//soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const {firstName, lastName, address, hobby} = newObject
console.log(firstName, lastName, address, hobby) //jawaban

//soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [...west, ...east]
console.log(combined) //jawaban

//soal 5
const planet = "earth" 
const view = "glass" 

var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
let after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet}`

console.log(after) //jawaban
