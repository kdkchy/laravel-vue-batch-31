# Batch 31
Pande Kadek Cahya Wisnu Murti

# Kadek Cahya Foundation - Crowdfunding Sanbercode
## Laravel 6 & Vue 2

## Tujuan Pembuatan Aplikasi
Pendalaman pemahaman terhadap VueJS yang dimana pembuatan API secara langsung dengan bantuan Laravel 6 sebagai backend nya. Dalam project ini telah dipelajari beberapa hal anatara lain:

    1. Penggunaan UUID sebagai primary key dalam setiap table
    2. Pemahaman Laravel Middleware yang memproteksi route 
    3. Pembuatan Autentikasi dengan menggunakan JWT pada Laravel
    4. Mail, Event, Listener & Queue
    5. Pembuatan Component, Router pada Vue
    6. Melakukan manajemen State dengan Vuex
    7. Menggunakan OAuth agar user dapat login dengn Google

Dalam pembuatan aplikasi ini dan hingga sampai saat ini sangat jauh dari sempurna, maka dari itu pengembang merekomendasikan untuk pengembangan aplikasi seperti :

    1. Login dengan Sosial Media (FB & Twitter)
    2. Donasi pembayaran menggunakan Midtrans
    3. Input history dan jumlah donasi pada database
    4. Buat sebuah page untuk pembacaan blog
    5. Fitur chat, live progress donasi & komen pada blog dengan menggunakan pusher

Demikian sedikit hal pengembang sampaikan, kali kedua pengembang mengikuti bootcamp yang diselenggarakan oleh Sanbercode, untuk itu pengembang ucapkan terimakasih kepada Sanbercode terutama kepada Mentor, Muhammad Iqbal Mubarok yang sudah membimbing sejauh ini.

## Berikut link yang bisa diakses untuk:
1. website kdkchy foundation, pada saat ini website masih belum berjalan dengn efektif. klik [disini](http://kdkchy-foundation.sanbercodeapp.com/)
2. link GDrive, video dan screenshoot aplikasi yang berjalan di localhost. klik [disini](https://drive.google.com/drive/folders/1yf7s0PHDLAk4s1c5yIMsBSR46GI-h1hj?usp=sharing)
