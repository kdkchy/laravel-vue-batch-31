import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'
import transactions from './module/transactions.js'
import alert from './module/alert.js'
import auth from './module/auth.js'
import dialog from './module/dialog.js'

const vuexPersist = new VuexPersist({
    key: 'crowdfunding-website',
    storage: window.localStorage
})

Vue.use(Vuex)

export default new Vuex.Store({
    plugins: [vuexPersist.plugin],
    modules : {
        transactions,
        alert,
        auth,
        dialog
    }
})