<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::group([
    'middleware' => ['api'],
    'prefix' => 'auth'

], function ($router) {
    Route::post('/login', 'AuthController@login')->name('login');
    Route::post('/register', 'AuthController@register')->name('register');
    Route::post('/verifikasi-otp', 'AuthController@verifikasiOtp')->name('verifikasi-otp');
    Route::post('/regenerate-otp', 'AuthController@regenerateOtp')->name('regenerate-otp');
    Route::post('/update-password', 'AuthController@updatePassword')->name('update-password');

    Route::post('/logout', 'AuthController@logout');
    Route::post('/refresh', 'AuthController@refresh');

    Route::post('/check-token', 'AuthController@checkToken');

    Route::get('/social/{provider}', 'SocialiteController@redirectToProvider');
    Route::get('/social/{provider}/callback', 'SocialiteController@handleProviderCallback');
    // Route::get('/user-profile', 'AuthController@userProfile');
    // Route::post('/update-profile', 'AuthController@updateProfile');    
});

Route::group([
    'middleware' => ['api'],
    'prefix' => 'profile'

], function ($router) {
    Route::get('/user-profile', 'AuthController@userProfile');
    Route::post('/update-profile', 'AuthController@updateProfile');    
});

Route::group([
    'middleware' => ['api'],
    'prefix' => 'campaign'

], function () {
    Route::get('random/{count}', 'CampaignController@random');
    Route::post('store', 'CampaignController@store');
    Route::get('/', 'CampaignController@index');
    Route::get('{id}', 'CampaignController@detail');
    Route::get('/search/{keyword}', 'CampaignController@search');
});

Route::group([
    'middleware' => ['api'],
    'prefix' => 'blog'

], function () {
    Route::get('random/{count}', 'BlogController@random');
    Route::post('store', 'BlogController@store');
});

