<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Route::middleware(['auth', 'admin'])->group(function () {
//     Route::get('/admin', function () {
//         return 'Selamat datang admin';
//     });
// });

// Route::middleware('verifiedEmail')->group(function () {
//     Route::get('/route-1', function () {
//         return 'Email terverifikasi';
//     });
// });

// Route::middleware(['auth', 'admin', 'verifiedEmail'])->group(function () {
//     Route::get('/route-2', function () {
//         return 'Email terverifikasi, selamat datang admin';
//     });
// });

// Route::get('{any}', function () {
//     return view('app');
// })->where('any','.*');

Route::view('/{any?}', 'app')->where('any','.*');