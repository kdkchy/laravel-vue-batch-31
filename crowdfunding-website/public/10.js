(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[10],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Donation.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Donation.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      campaigns: []
    };
  },
  methods: {
    go: function go() {
      var _this = this;

      var campaigns_arr = JSON.parse(localStorage.getItem("campaigns"));

      if (campaigns_arr) {
        campaigns_arr.forEach(function (element) {
          var url = '/api/campaign/' + element;
          axios.get(url).then(function (response) {
            var data = response.data.data;

            _this.campaigns.push(response.data.data.campaign);
          })["catch"](function (error) {
            var response = error.response;
            console.log(response);
          });
        });
      }
    }
  },
  mounted: function mounted() {
    this.go();
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Donation.vue?vue&type=template&id=ed8d1a9e&":
/*!******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Donation.vue?vue&type=template&id=ed8d1a9e& ***!
  \******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm.campaigns.length != 0
      ? _c(
          "div",
          _vm._l(_vm.campaigns, function (campaign, index) {
            return _c(
              "v-card",
              {
                key: index,
                staticClass: "mb-2",
                attrs: { to: "/campaign/" + campaign.id },
              },
              [
                campaign.id
                  ? _c(
                      "v-card",
                      [
                        _c(
                          "v-img",
                          {
                            staticClass: "white--tetx",
                            attrs: { src: campaign.image, height: "200px" },
                          },
                          [
                            _c("v-card-title", {
                              staticClass: "fill-height align-end",
                              domProps: { textContent: _vm._s(campaign.title) },
                            }),
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "v-card-text",
                          [
                            _c("v-simple-table", { attrs: { dense: "" } }, [
                              _c("tbody", [
                                _c("tr", [
                                  _c(
                                    "td",
                                    [
                                      _c("v-icon", [_vm._v("mdi-home-city")]),
                                      _vm._v(
                                        " Alamat\n                            "
                                      ),
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c("td", [
                                    _vm._v(
                                      "\n                                " +
                                        _vm._s(campaign.address) +
                                        "\n                            "
                                    ),
                                  ]),
                                ]),
                                _vm._v(" "),
                                _c("tr", [
                                  _c(
                                    "td",
                                    [
                                      _c("v-icon", [_vm._v("mdi-home-heart")]),
                                      _vm._v(
                                        " Terkumpul\n                            "
                                      ),
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "blue--text" }, [
                                    _vm._v(
                                      "\n                                Rp " +
                                        _vm._s(
                                          parseInt(
                                            campaign.collected
                                          ).toLocaleString("id-ID")
                                        ) +
                                        "\n                            "
                                    ),
                                  ]),
                                ]),
                                _vm._v(" "),
                                _c("tr", [
                                  _c(
                                    "td",
                                    [
                                      _c("v-icon", [_vm._v("mdi-cash")]),
                                      _vm._v(
                                        " Dibutuhkan\n                            "
                                      ),
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c("td", { staticClass: "orange--text" }, [
                                    _vm._v(
                                      "\n                                Rp " +
                                        _vm._s(
                                          parseInt(
                                            campaign.required
                                          ).toLocaleString("id-ID")
                                        ) +
                                        "\n                            "
                                    ),
                                  ]),
                                ]),
                              ]),
                            ]),
                          ],
                          1
                        ),
                      ],
                      1
                    )
                  : _vm._e(),
              ],
              1
            )
          }),
          1
        )
      : _c(
          "div",
          [
            _c(
              "v-card-text",
              [
                _c(
                  "v-alert",
                  {
                    attrs: {
                      value: true,
                      type: "info",
                      icon: "mdi-alert-circle-outline",
                    },
                  },
                  [
                    _vm._v(
                      "\n                    Belum ada history donasi. Lakukan donasi untuk menambahkan history donasi.\n                "
                    ),
                  ]
                ),
              ],
              1
            ),
          ],
          1
        ),
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/Donation.vue":
/*!*****************************************!*\
  !*** ./resources/js/views/Donation.vue ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Donation_vue_vue_type_template_id_ed8d1a9e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Donation.vue?vue&type=template&id=ed8d1a9e& */ "./resources/js/views/Donation.vue?vue&type=template&id=ed8d1a9e&");
/* harmony import */ var _Donation_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Donation.vue?vue&type=script&lang=js& */ "./resources/js/views/Donation.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Donation_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Donation_vue_vue_type_template_id_ed8d1a9e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Donation_vue_vue_type_template_id_ed8d1a9e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Donation.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Donation.vue?vue&type=script&lang=js&":
/*!******************************************************************!*\
  !*** ./resources/js/views/Donation.vue?vue&type=script&lang=js& ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Donation_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Donation.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Donation.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Donation_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Donation.vue?vue&type=template&id=ed8d1a9e&":
/*!************************************************************************!*\
  !*** ./resources/js/views/Donation.vue?vue&type=template&id=ed8d1a9e& ***!
  \************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Donation_vue_vue_type_template_id_ed8d1a9e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Donation.vue?vue&type=template&id=ed8d1a9e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Donation.vue?vue&type=template&id=ed8d1a9e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Donation_vue_vue_type_template_id_ed8d1a9e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Donation_vue_vue_type_template_id_ed8d1a9e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);