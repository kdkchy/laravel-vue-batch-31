(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[8],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Campaign.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Campaign.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      campaign: {},
      storedCampaigns: []
    };
  },
  created: function created() {
    this.go();
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])({
    guest: 'auth/guest'
  })),
  methods: _objectSpread(_objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapMutations"])({
    tambahTransaksi: 'transactions/insert'
  })), Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])({
    setAlert: 'alert/set',
    setDialogComponent: 'dialog/setComponent'
  })), {}, {
    donate: function donate() {
      if (this.guest) {
        this.setDialogComponent('login');
        this.setAlert({
          status: true,
          text: 'Login untuk donasi',
          color: 'success'
        });
      } else {
        this.tambahTransaksi(); //nambah donasi ke lokal storage

        var temp = [];
        var campaigns = JSON.parse(localStorage.getItem("campaigns"));

        if (campaigns) {
          campaigns.push(this.campaign.id);
          localStorage.setItem("campaigns", JSON.stringify(campaigns));
        } else {
          temp.push(this.campaign.id);
          localStorage.setItem("campaigns", JSON.stringify(temp));
        }

        this.setAlert({
          status: true,
          text: 'Terima kasih telah menyumbang',
          color: 'success'
        });
      }
    },
    go: function go() {
      var _this = this;

      var id = this.$route.params.id;
      var url = '/api/campaign/' + id;
      axios.get(url).then(function (response) {
        var data = response.data.data;
        _this.campaign = data.campaign;
      })["catch"](function (error) {
        var response = error.response;
        console.log(error);
      });
    }
  })
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Campaign.vue?vue&type=template&id=5561fd2f&":
/*!******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Campaign.vue?vue&type=template&id=5561fd2f& ***!
  \******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _vm.campaign.id
        ? _c(
            "v-card",
            [
              _c(
                "v-img",
                {
                  staticClass: "white--tetx",
                  attrs: { src: _vm.campaign.image, height: "200px" },
                },
                [
                  _c("v-card-title", {
                    staticClass: "fill-height align-end",
                    domProps: { textContent: _vm._s(_vm.campaign.title) },
                  }),
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-card-text",
                [
                  _c("v-simple-table", { attrs: { dense: "" } }, [
                    _c("tbody", [
                      _c("tr", [
                        _c(
                          "td",
                          [
                            _c("v-icon", [_vm._v("mdi-home-city")]),
                            _vm._v(" Alamat\n                        "),
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c("td", [
                          _vm._v(
                            "\n                            " +
                              _vm._s(_vm.campaign.address) +
                              "\n                        "
                          ),
                        ]),
                      ]),
                      _vm._v(" "),
                      _c("tr", [
                        _c(
                          "td",
                          [
                            _c("v-icon", [_vm._v("mdi-home-heart")]),
                            _vm._v(" Terkumpul\n                        "),
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c("td", { staticClass: "blue--text" }, [
                          _vm._v(
                            "\n                            Rp " +
                              _vm._s(
                                parseInt(_vm.campaign.collected).toLocaleString(
                                  "id-ID"
                                )
                              ) +
                              "\n                        "
                          ),
                        ]),
                      ]),
                      _vm._v(" "),
                      _c("tr", [
                        _c(
                          "td",
                          [
                            _c("v-icon", [_vm._v("mdi-cash")]),
                            _vm._v(" Dibutuhkan\n                        "),
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c("td", { staticClass: "orange--text" }, [
                          _vm._v(
                            "\n                            Rp " +
                              _vm._s(
                                parseInt(_vm.campaign.required).toLocaleString(
                                  "id-ID"
                                )
                              ) +
                              "\n                        "
                          ),
                        ]),
                      ]),
                    ]),
                  ]),
                  _vm._v("\n            Description: "),
                  _c("br"),
                  _vm._v(" " + _vm._s(_vm.campaign.description) + "\n        "),
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-card-actions",
                [
                  _c(
                    "v-btn",
                    {
                      attrs: {
                        block: "",
                        color: "primary",
                        disabled:
                          _vm.campaign.collected >= _vm.campaign.required,
                      },
                      on: { click: _vm.donate },
                    },
                    [
                      _c("v-icon", [_vm._v("mdi-money")]),
                      _vm._v("   Donate\n            "),
                    ],
                    1
                  ),
                ],
                1
              ),
            ],
            1
          )
        : _vm._e(),
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/Campaign.vue":
/*!*****************************************!*\
  !*** ./resources/js/views/Campaign.vue ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Campaign_vue_vue_type_template_id_5561fd2f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Campaign.vue?vue&type=template&id=5561fd2f& */ "./resources/js/views/Campaign.vue?vue&type=template&id=5561fd2f&");
/* harmony import */ var _Campaign_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Campaign.vue?vue&type=script&lang=js& */ "./resources/js/views/Campaign.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Campaign_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Campaign_vue_vue_type_template_id_5561fd2f___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Campaign_vue_vue_type_template_id_5561fd2f___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Campaign.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Campaign.vue?vue&type=script&lang=js&":
/*!******************************************************************!*\
  !*** ./resources/js/views/Campaign.vue?vue&type=script&lang=js& ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Campaign_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Campaign.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Campaign.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Campaign_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Campaign.vue?vue&type=template&id=5561fd2f&":
/*!************************************************************************!*\
  !*** ./resources/js/views/Campaign.vue?vue&type=template&id=5561fd2f& ***!
  \************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Campaign_vue_vue_type_template_id_5561fd2f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Campaign.vue?vue&type=template&id=5561fd2f& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Campaign.vue?vue&type=template&id=5561fd2f&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Campaign_vue_vue_type_template_id_5561fd2f___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Campaign_vue_vue_type_template_id_5561fd2f___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);