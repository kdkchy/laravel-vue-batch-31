<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\OtpCode;
use Validator;
use App\Events\UserRegistrationEvent;
use App\Events\UserRegenerateOtpEvent;

class AuthController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['login', 'register', 'verifikasiOtp', 'regenerateOtp', 'updatePassword']]);
    }

    public function login(Request $request){
    	$validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);

        if (!$user = User::where('email', $request->email)->whereNotNull('email_verified_at')->first()) {
            return response()->json([
                'response_code' => '01',
                'message' => 'Email belum terverifikasi',
                'status' => 'error'
            ], 401);
        }

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        if (! $token = auth()->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->createNewToken($token);
    }

    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            // 'username' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            // 'password' => 'required|string|confirmed|min:6',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->first(), 400);
        }

        $user = User::create(array_merge(
                    $validator->validated(),
                    [   'username' => $request->email,
                        'role_id' => 'e34c625e-2727-43d2-a2fd-60866f7e96c5',
                        'password' => 'unconfirmed',
                    ]
                ));
        
        $otpCode = OtpCode::create([
            'user_id' => $user->id,
            'otp' => random_int(100000, 999999),
            'valid_until' => now()->addMinutes(5),
        ]);

        event(new UserRegistrationEvent($user, $otpCode));

        return response()->json([
            'response_code' => '00',
            'message' => 'User successfully registered, check your OTP on email',
        ], 201);
    }

    public function verifikasiOtp(Request $request) {
        $otpCode = OtpCode::where('otp', $request->otp)->first();
        $user = User::where('id', $otpCode->user_id)->first();

        if($otpCode->valid_until < now()){
            return response()->json([
                'response_code' => '01',
                'message' => 'OTP code expired'
            ], 401);
        }

        $user->update([
            // 'password' => bcrypt($otpCode->otp),
            'email_verified_at' => \Carbon\Carbon::now(),
        ]);

        $otpCode->delete();

        return response()->json([
            'response_code' => '00',
            'message' => 'OTP code successfully verified'
        ], 200);
    }

    public function regenerateOtp(Request $request){
        $user = User::where('email', $request->email)->first();

        $otpCode = OtpCode::where('user_id', $user->id)
                ->update([
                    'otp' => random_int(100000, 999999),
                    'valid_until' => now()->addMinutes(5)
                ]);
        $getOtp = OtpCode::where('user_id', $user->id)->first();
        event(new UserRegenerateOtpEvent($user, $getOtp));
        
        return response()->json([
            'response_code' => '00',
            'message' => 'OTP code successfully regenerated, check your OTP on email',
        ], 200);
    }

    public function updatePassword(Request $request){
        $validator = Validator::make($request->all(), [
            'password' => 'required|string|confirmed|min:6',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = User::where('email', $request->email)->first();

        $user->update([
            'password' => bcrypt($request->password),
        ]);

        return response()->json([
            'response_code' => '00',
            'message' => 'Password successfully updated'
        ], 200);
    }

    public function logout() {
        auth()->logout();

        return response()->json([
            'response_code' => '00',
            'message' => 'User successfully signed out'
        ]);
    }

    public function refresh() {
        return $this->createNewToken(auth()->refresh());
    }

    public function userProfile() {
        return response()->json(auth()->user());
    }

    protected function createNewToken($token){
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }

    public function updateProfile(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = User::where('id', auth()->user()->id)->first();

        if($request->hasFile('photo')){
            $image = $request->file('photo');
            $name = '/photo/users/photo-profile/'.auth()->user()->id.'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/photo/users/photo-profile');
            $image->move($destinationPath, $name);
            $user->update([
                'name' => $request->name,
                'photo' => $name,
            ]);
        }

        return response()->json([
            'response_code' => '00',
            'message' => 'Profile successfully updated',
            'user' => $user,
        ], 200);
    }

    public function checkToken(Request $request){
        return response()->json([
            'response_code' => '00',
            'message' => 'token_valid',
            'data' => true,
        ], 200);
    }
}