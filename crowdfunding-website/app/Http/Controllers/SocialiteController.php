<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\User;
use Carbon\Carbon;

class SocialiteController extends Controller
{
    public function redirectToProvider($provider){
        $url = Socialite::driver($provider)->stateless()->redirect()->getTargetUrl();

        return response()->json([
            'url' => $url
        ]);
    }

    public function handleProviderCallback($provider){
        try {
            $social_user = Socialite::driver($provider)->stateless()->user();

            if(!$social_user){
                return response()->json([
                    'response_code' => '01',
                    'message' => 'User not found',
                    'status' => 'error'
                ], 401);
            }

            $user = User::whereEmail($social_user->email)->first();

            if(!$user){
                if($provider == 'google'){
                    $photo_profile = $social_user->avatar;
                }
                $user = User::create([
                    'email' => $social_user->email,
                    'name' => $social_user->name,
                    'email_verified_at' => Carbon::now(),
                    'photo' => $photo_profile
                ]);
            }

            return response()->json([
                'response_code' => '00',
                'message' => 'Success',
                'status' => 'success',
                'user' => $user,
                'access_token' => auth()->login($user)
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'response_code' => '01',
                'message' => 'login failed',
            ], 401);
        }
    }
}
