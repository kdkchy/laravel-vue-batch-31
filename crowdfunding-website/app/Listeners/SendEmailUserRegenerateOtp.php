<?php

namespace App\Listeners;

use App\Events\UserRegenerateOtpEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\UserRegenerateOtp;
use Mail;

class SendEmailUserRegenerateOtp implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegenerateOtpEvent  $event
     * @return void
     */
    public function handle(UserRegenerateOtpEvent $event)
    {
        Mail::to($event->user)->send(new UserRegenerateOtp($event->user, $event->otpCode));
    }
}
