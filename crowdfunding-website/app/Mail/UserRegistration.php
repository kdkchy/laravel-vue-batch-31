<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;
use App\OtpCode;

class UserRegistration extends Mailable
{
    use Queueable, SerializesModels;


    protected $user;
    protected $otp;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, OtpCode $otp)
    {
        $this->user = $user;
        $this->otp = $otp;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return 
            $this->from('kdkchy@gmail.com')
            ->view('emails.registration')
            ->with([
                'name' => $this->user->name,
                'otp' => $this->otp->otp
            ]);
    }
}
