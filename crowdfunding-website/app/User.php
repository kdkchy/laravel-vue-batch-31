<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\Factories\HasFactory;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Auth;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'username', 'role_id', 'password', 'email_verified_at', 'photo'
    ];


    protected $attributes = [
        'password' => 'unconfirmed',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];

    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if( empty($model->{ $model->getKeyName() }) ){
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
            $model->role_id = 'e34c625e-2727-43d2-a2fd-60866f7e96c5';
            $model->username = $model->email;
        });
    }

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function otp()
    {
        return $this->hasOne('App\OtpCode');
    }

    public function isAdmin()
    {
        if($this->role_id == 'e2a45584-7151-4b4c-a572-233401d19443')
        {   
            return true;
        }
        return false;
    }

    public function isEmailVerified()
    {
        if($this->email_verified_at != NULL)
        {   
            return true;
        }
        return false;
    }

    public function getJWTIdentifier() {
        return $this->getKey();
    }

    public function getJWTCustomClaims() {
        return [];
    }
}
